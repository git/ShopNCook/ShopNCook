﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopNCook.Controllers
{
    public interface LoginController
    {
        public void Login(string email, string password);
    }
}
