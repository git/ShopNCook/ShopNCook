﻿using Services;
using Models;

namespace ShopNCook.Controllers
{
    public class ConnectionController : LoginController, RegisterController
    {
        private readonly ConnectionObserver observer;
        private readonly IAuthService accounts;
        public ConnectionController(ConnectionObserver observer, IAuthService accounts) {
            this.observer = observer;
            this.accounts = accounts;
        }

        public void Login(string email, string password)
        {
            if (email == null)
            {
                UserNotifier.Notice("Please provide an email address");
                return;
            }
            if (password == null)
            {
                UserNotifier.Notice("Please provide your password");
                return;
            }
            Account? acc = accounts.Login(email, password);    
            if (acc == null)
            {
                UserNotifier.Error("Email or password invalid.");
                return;
            }
            observer.OnAccountConnected(acc);
        }

        public void Register(string username, string email, string password)
        {
            if (email == null)
            {
                UserNotifier.Notice("Please provide an email address");
                return;
            }
            if (password == null)
            {
                UserNotifier.Notice("Please provide your password");
                return;
            }
            if (username == null)
            {
                UserNotifier.Notice("Please provide an username");
                return;
            }
            Account? acc = accounts.Register(username, email, password);
            if (acc == null)
            {
                UserNotifier.Error("Invalid credentials.");
                return;
            }
            observer.OnAccountConnected(acc);
        }
    }
}
