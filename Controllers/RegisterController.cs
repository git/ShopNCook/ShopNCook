﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopNCook.Controllers
{
    public interface RegisterController
    {
        public void Register(string username, string email, string password);
    }
}
