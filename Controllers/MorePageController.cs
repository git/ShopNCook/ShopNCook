﻿using Services;
using Models;
using ShopNCook.Pages;

namespace ShopNCook.Controllers
{
    public class MorePageController
    {

        private readonly IApp app;
        private readonly IEndpoint endpoint;
        private readonly Account account;

        public MorePageController(Account account, IEndpoint endpoint, IApp app)
        {
            this.app = app;
            this.endpoint = endpoint;
            this.account = account;
        }

        public void Logout()
        {
            UserNotifier.Notice("You have been logged out.");
            app.ForceLogin();
        }

        public async void GoToMyRecipesPage()
        {
            await Shell.Current.Navigation.PushAsync(new MyRecipesPage(account, endpoint.RecipesService));
        }

        public async void GoToProfilePage()
        {
            await Shell.Current.Navigation.PushAsync(new ProfilePage(account));
        }
    }
}
