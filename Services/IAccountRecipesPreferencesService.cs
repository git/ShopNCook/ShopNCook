﻿using Models;
using System.Collections.Immutable;

namespace Services
{
    /// <summary>
    /// This service handles the preferences of a bound account
    /// </summary>
    public interface IAccountRecipesPreferencesService
    {
        /// <summary>
        /// The bound account
        /// </summary>
        public Account Account { get; }

        /// <summary>
        /// Adds a recipe in the favorites of the bound account
        /// </summary>
        /// <param name="info">The information about the recipe to add in favorites</param>
        public void AddToFavorites(RecipeInfo info);
        /// <summary>
        /// Removes a recipe from the favorites of the bound account
        /// </summary>
        /// <param name="info">The information about the recipe to remove from favorites</param>
        public void RemoveFromFavorites(RecipeInfo info);
        /// <summary>
        /// Sets a score for the specified recipe
        /// </summary>
        /// <param name="info">The information about the targeted recipe</param>
        /// <param name="score">The score to set</param>
        public void SetReviewScore(RecipeInfo info, uint score);
        /// <summary>
        /// Adds a recipe to the weekly list, specifying the amount of persons that must be fed for the week
        /// </summary>
        /// <param name="info">The information about the targeted recipe</param>
        /// <param name="persAmount">The amount of guests that needs to be fed by the recipe for the week</param>
        /// <returns></returns>
        public bool AddToWeeklyList(RecipeInfo info, uint persAmount);

        /// <summary>
        /// Retrieves the rate of the targeted recipe
        /// The rate contains the user's score and whether if the recipe is in the favorites list.
        /// <see cref="RecipeInfo"/>
        /// </summary>
        /// <param name="info">The information about the targeted recipe</param>
        /// <returns></returns>
        public RecipeRate GetRate(RecipeInfo info);

        /// <summary>
        /// The favorites recipes of the account
        /// </summary>
        /// <returns>A list containing all the recipe info that are marked as favorite by the bound account</returns>
        public ImmutableList<RecipeInfo> GetFavorites();

        /// <summary>
        /// The recommended recipes for the user based on his preferences.
        /// </summary>
        /// <returns>A list of the recommended recipes based on the preferences of the bound account</returns>
        public ImmutableList<RecipeInfo> GetRecommendedRecipes();

        /// <summary>
        /// The weekly list of the bound account
        /// </summary>
        /// <returns>The weekly list of the bound account, containing tuples that binds a recipe to the number of guests to feed for the week</returns>
        public ImmutableList<(RecipeInfo, uint)> GetWeeklyList();

    }
}
