﻿

namespace Services
{
    /// <summary>
    /// The endpoint is the central element of the 'Services' assembly.
    /// </summary>
    public interface IEndpoint
    {
        public IAuthService AuthService { get; }

        public IRecipesService RecipesService { get; }

    }
}

