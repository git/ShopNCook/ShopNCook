﻿using Models;

namespace Services
{
    /// <summary>
    /// Service for account authentification
    /// Passwords are being passed in this service clearly as the hash function used for passwords is implementation specific 
    /// </summary>
    public interface IAuthService
    {
        /// <summary>
        /// Tries to login to an account using its mail address and password.
        /// </summary>
        /// <param name="email"> The mail address which acts as an identifier for the targeted account </param>
        /// <param name="password"> The (clear) password used to login.</param>
        /// <returns>
        /// Returns an instance of Account representing the account that got logged in.
        /// If the login credentials are invalid to log in the targeted acccount, this method returns null.
        /// </returns>
        public Account? Login(string email, string password);
        /// <summary>
        /// Tries to register to a new account, defining its mail address, username and password.
        /// </summary>
        /// <param name="email"> The mail address which acts as an identifier for the targeted account </param>
        /// <param name="username"> The username of the account </param>
        /// <param name="password"> The (clear) password used to login on next connections attempt.</param>
        /// <returns>
        /// Returns an instance of Account representing the account that got newly registered.
        /// If the register credentials are invalid, or if the account already exists, this method returns null.
        /// </returns>
        public Account? Register(string email, string username, string password);
    }
}

