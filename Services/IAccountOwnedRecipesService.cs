﻿using Models;
using System.Collections.Immutable;

namespace Services
{
    /// <summary>
    /// This service handles the recipes created by an account
    /// </summary>
    public interface IAccountOwnedRecipesService
    {
        /// <summary>
        /// This service's bound account
        /// </summary>
        public Account Account { get; }

        /// <summary>
        /// Upload a new recipe, ensuring the recipe's owner matches the service's bound account user.
        /// </summary>
        /// <param name="recipe">The recipe to upload</param>
        /// <returns>true if the recipe could be uploaded, false instead</returns>
        public bool UploadRecipe(Recipe recipe);

        /// <summary>
        /// Removes a recipe
        /// </summary>
        /// <param name="info">The informations about the recipe to remove</param>
        /// <returns>true if the recipe could be removed, false instead</returns>
        public bool RemoveRecipe(RecipeInfo info);

        /// <summary>
        /// The living recipes created by this account.
        /// If the user removes a recipe (using <see cref="RemoveRecipe(RecipeInfo)"/>) it'll no longer apear in the 
        /// next invocations of this recipe
        /// </summary>
        /// <returns>the list of all the living recipes of the account</returns>
        public ImmutableList<RecipeInfo> GetAccountRecipes();
    }
}
