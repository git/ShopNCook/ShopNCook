﻿using Models;
using System.Collections.Immutable;

namespace Services
{
    /// <summary>
    /// The services that is in charge of handling the application's recipes.
    /// </summary>
    public interface IRecipesService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns>A list containg the popular recipes of the week</returns>
        public ImmutableList<RecipeInfo> PopularRecipes();

        /// <summary>
        /// performs a search over all the recipes
        /// </summary>
        /// <returns>A list containg the recipes that matches the prompt</returns>
        public ImmutableList<RecipeInfo> SearchRecipes(string prompt);

        /// <summary>
        /// Retrieves a recipe from given RecipeInfo
        /// </summary>
        /// <param name="info">the informations about the recipe that we want to retrieve</param>
        /// <returns>some recipe if the recipe was found, null else</returns>
        public Recipe? GetRecipe(RecipeInfo info);

        /// <summary>
        /// Gets the service that is in charge of handling the account's owned recipes.
        /// The account's owned recipes are the recipes that the account created.
        /// </summary>
        /// <param name="account">The account logged in</param>
        /// <returns>The service that handles the given account's recipes</returns>
        public IAccountOwnedRecipesService GetRecipesOf(Account account);
        /// <summary>
        /// Gets the service that handles all the preferences of the given account
        /// </summary>
        /// <param name="account">The account logged in</param>
        /// <returns>
        /// The service that handles the given account's preferences
        /// </returns>
        public IAccountRecipesPreferencesService GetPreferencesOf(Account account);


    }
}
