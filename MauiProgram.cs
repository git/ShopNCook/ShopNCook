﻿using CommunityToolkit.Maui;
using Microsoft.Extensions.Logging;

namespace ShopNCook;

public static class MauiProgram
{
	public static MauiApp CreateMauiApp()
	{
		var builder = MauiApp.CreateBuilder();
		builder
			.UseMauiApp<App>()
			.UseMauiCommunityToolkit()
			.ConfigureFonts(fonts =>
			{
				fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
				fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
				fonts.AddFont("Poppins-Bold.ttf", "PoppinsBold");
				fonts.AddFont("Poppins-Regular.ttf", "Poppins");
				fonts.AddFont("Poppins-Medium.ttf", "PoppinsMedium");
            });
#if DEBUG
		builder.Logging.AddDebug();
#endif

		return builder.Build();
	}
}
