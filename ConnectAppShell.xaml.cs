﻿namespace ShopNCook;
using Microsoft.Maui.Controls;
using Services;
using ShopNCook.Controllers;
using ShopNCook.Pages;


// Shell pour la phase de connexion de l'application
public partial class ConnectAppShell : Shell
{
    // Constructeur qui prend un observateur de connexion et un service d'authentification en argument

    public ConnectAppShell(ConnectionObserver observer, IAuthService accounts)
	{
        // Création d'un nouveau contrôleur de connexion
        ConnectionController controller = new ConnectionController(observer, accounts);
		InitializeComponent();
        
        // Initialisation des pages de connexion et d'inscription avec le contrôleur de connexion
        LoginPage.ContentTemplate = new DataTemplate(() => new LoginPage(controller));
		RegisterPage.ContentTemplate = new DataTemplate(() => new RegisterPage(controller));
    }
}
