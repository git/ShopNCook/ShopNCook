﻿using CommunityToolkit.Maui.Alerts;
using CommunityToolkit.Maui.Core;


namespace ShopNCook
{
    internal class UserNotifier
    {
        private static async Task Show(string message, string messageType)
        {
            CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();

            // Vous pouvez configurer la durée et la taille de police ici.
            ToastDuration duration = ToastDuration.Short;
            double fontSize = 14;

            var toast = Toast.Make(message, duration, fontSize);

            await toast.Show(cancellationTokenSource.Token);
        }



        public static void Error(string message)
        {
            Show(message, "Error");
        }
        public static void Warn(string message)
        {
            Show(message, "Warning");
        }
        public static void Notice(string message)
        {
            Show(message, "Notice");
        }
        public static void Success(string message)
        {
            Show(message, "Success");
        }
    }

}
