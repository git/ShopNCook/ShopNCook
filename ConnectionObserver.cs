﻿using Models;

namespace ShopNCook
{
    // Interface définissant un observateur de connexion.
    // Tout objet implémentant cette interface doit définir la méthode OnAccountConnected().
    public interface ConnectionObserver
    {
        public void OnAccountConnected(Account account);
    }
}
