﻿using System.Runtime.Serialization;

namespace Models
{

    /// <summary>
    /// An ingredient
    /// </summary>
    /// <param name="Name">The ingredient's name</param>
    /// <param name="Amount">The ingredient's amount (in kilograms or liters)</param>
    [DataContract]
    public record Ingredient([property: DataMember] string Name, [property: DataMember] float Amount, [property: DataMember] string Unit);
}
