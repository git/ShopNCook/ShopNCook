﻿using System.Runtime.Serialization;

namespace Models
{
    /// <summary>
    /// The essential information about a recipe
    /// </summary>
    /// <param name="Name">The recipe's name</param>
    /// <param name="CalPerPers">The energy input</param>
    /// <param name="CookTimeMins">Estimated time of preparation in minutes</param>
    /// <param name="Image">An illustrative image of the recipe</param>
    /// <param name="AverageNote">The average rate of the recipe</param>
    /// <param name="Id">An unique identifier</param>
    [DataContract]
    public record RecipeInfo(
        [property: DataMember] string Name,
        [property: DataMember] uint CalPerPers,
        [property: DataMember] uint CookTimeMins,
        [property: DataMember] Uri Image,
        [property: DataMember] float AverageNote,
        [property: DataMember] Guid Id
    );
}
