﻿using System.Collections.Immutable;
using System.Runtime.Serialization;

namespace Models
{
    /// <summary>
    /// A Recipe
    /// </summary>
    /// <param name="Info">The essential information of the recipe</param>
    /// <param name="Owner">The creator of the recipe</param>
    /// <param name="Ingredients">The needed ingredients of the recipe</param>
    /// <param name="Steps">The preparation steps</param>
    [DataContract]
    public record Recipe(
        [property: DataMember] RecipeInfo Info,
        [property: DataMember] User Owner,
        [property: DataMember] ImmutableList<Ingredient> Ingredients,
        [property: DataMember] ImmutableList<PreparationStep> Steps
    );
}