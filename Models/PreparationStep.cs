﻿using System.Runtime.Serialization;

namespace Models
{
    /// <summary>
    /// A step of preparation
    /// </summary>
    /// <param name="Name">The step's name</param>
    /// <param name="Description">The step's instructions / description</param>
    [DataContract]
    public record PreparationStep([property: DataMember] string Name, [property: DataMember] string Description);
}
