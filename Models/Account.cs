﻿namespace Models
{
    /// <summary>
    /// Contains the informations of an account.
    /// </summary>
    /// <param name="User">The account's public information</param>
    /// <param name="Email">The account's email address</param>
    public record Account(User User, string Email);
}
