﻿
using System.Runtime.Serialization;

namespace Models
{
    /// <summary>
    /// The rate of a recipe, usually, the instances are bound with an account.
    /// </summary>
    /// <param name="IsFavorite"></param>
    /// <param name="Rate">a rate between 0 and 5</param>
    [DataContract]
    public record RecipeRate(
        [property: DataMember] bool IsFavorite = false,
        [property: DataMember] uint Rate = 0
    );
}
