﻿using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models
{
    /// <summary>
    /// A Simple builder to create a recipe
    /// </summary>
    public class RecipeBuilder
    {
        private readonly string name;
        private readonly User owner;

        private uint callPerPers;
        private uint cookTimeMins;
        private Uri? image;

        private List<Ingredient> ingredients = new List<Ingredient>();
        private List<PreparationStep> steps = new List<PreparationStep>();


        public RecipeBuilder(string name, User owner) 
        {
            this.name = name;
            this.owner = owner;
        }

        public RecipeBuilder SetCallPerPers(uint callPerPers)
        {
            this.callPerPers = callPerPers;
            return this;
        }

        public RecipeBuilder SetCookTimeMins(uint cookTimeMins)
        {
            this.cookTimeMins = cookTimeMins;
            return this;
        }

        public RecipeBuilder SetImage(Uri image)
        {
            this.image = image;
            return this;
        }

        public RecipeBuilder AddIngredient(Ingredient ingredient)
        {
            this.ingredients.Add(ingredient);
            return this;
        }

        public RecipeBuilder AddStep(PreparationStep step)
        {
            this.steps.Add(step);
            return this;
        }

        public Recipe Build()
        {
            RecipeInfo info = new RecipeInfo(name, callPerPers, cookTimeMins, image, 0, Guid.NewGuid());
            return new Recipe(info, owner, ingredients.ToImmutableList(), steps.ToImmutableList());
        }
    }
}
