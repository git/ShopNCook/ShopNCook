﻿using System.Runtime.Serialization;

namespace Models
{
    /// <summary>
    /// Publics informations of a user
    /// </summary>
    [DataContract]
    public class User
    {
        /// <summary>
        /// The profile picture
        /// </summary>
        [DataMember]
        public Uri ProfilePicture { get; init; }
        /// <summary>
        /// The username
        /// </summary>
        [DataMember]
        public string Name { get; init; }
        /// <summary>
        /// An unique identifier
        /// </summary>
        [DataMember]
        public Guid Id { get; init; }

        public User(Uri profilePicture, string name, Guid id)
        {
            ProfilePicture = profilePicture;
            Name = name;
            Id = id;
        }

        public override bool Equals(object? other)
        {
            if (this == other) 
                return true;

            User? otherUser = other as User;
            return otherUser != null && Id.Equals(otherUser.Id);
        }

        override public int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
