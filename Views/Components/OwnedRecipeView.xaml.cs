using Models;

namespace ShopNCook.Views;

public partial class OwnedRecipeView : ContentView
{

    private readonly Action clickCallback;
    private readonly Action removeCallback;
    private readonly RecipeInfo recipeInfo;

    public OwnedRecipeView(RecipeInfo info, Action onClickCallback, Action onRemoveCallback)
    {
        InitializeComponent();

        Note = info.AverageNote;

        BindingContext = info;
        this.clickCallback = onClickCallback;
        this.removeCallback = onRemoveCallback;
        this.recipeInfo = info;
    }

    public bool IsViewing(RecipeInfo info)
    {
        return recipeInfo == info;
    }
    public float Note
    {
        set => SetNote(value);
    }

   
    private void SetNote(float note)
    {
        note = (uint)note; //truncate integer as we currently do not handle semi stars
        foreach (Image img in Stars.Children.Reverse())
        {
            if (note > 0)
            {
                img.Opacity = 1;
                note--;
            }
            else img.Opacity = 0;
        }
    }

    private void OnViewTapped(object sender, TappedEventArgs e)
    {
        clickCallback();
    }

    private void OnRemoveButtonTapped(object sender, TappedEventArgs e)
    {
        removeCallback();
    }
}