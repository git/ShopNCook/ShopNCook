using Models;

namespace ShopNCook.Views;

// Classe repr�sentant une entr�e d'ingr�dient
public partial class IngredientEntry : ContentView
{

    public string QuantityText { get; set; }
    public string NameText { get; set; }

    public IngredientEntry()
    {
        BindingContext = this;
        InitializeComponent();
    }

    // Renvoie une nouvelle instance de Ingredient � partir des informations entr�es par l'utilisateur
    public Ingredient? MakeValue()
    {
        float quantity;

        // Tente de convertir la quantit� en float, sinon, attribue une valeur par d�faut de 0
        if (!float.TryParse(QuantityText, out quantity) || quantity < 0)
        {
            UserNotifier.Error("La quantit� doit �tre un nombre positif");
            return null;
        }

        return new Ingredient(NameText, quantity, UnitPicker.SelectedItem as string);
    }
}
