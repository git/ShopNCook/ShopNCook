using Models;

namespace ShopNCook.Views
{
    public partial class RecipeView : ContentView
    {
        private readonly Action callback;
        public RecipeInfo Info { get; private init; }

        public RecipeView(RecipeInfo info, Action callback)
        {
            this.callback = callback;
            Info = info;
            BindingContext = info;
            InitializeComponent();
            Note = info.AverageNote;
        }

        public float Note
        {
            set => SetNote(value);
        }


        private void SetNote(float note)
        {
            note = (uint)note; //truncate integer as we currently do not handle semi stars
            foreach (Image img in Stars.Children.Reverse())
            {
                if (note > 0)
                {
                    img.Opacity = 1;
                    note--;
                }
                else img.Opacity = 0;
            }
        }

        private void OnRecipeTapped(object sender, EventArgs e)
        {
            callback();
        }
    }
}
