using Models;

namespace ShopNCook.Views;

public partial class StepEntry : ContentView
{
	
	public StepEntry(): this(1) 
	{}

	public StepEntry(uint ordinal)
	{
		InitializeComponent();
		Ordinal = ordinal;
	}

	public PreparationStep MakeStep()
	{
		return new PreparationStep("Step " + Ordinal, StepEditor.Text);
	}

	public uint Ordinal { 
		get => uint.Parse(OrdinalLabel.Text);
		set => OrdinalLabel.Text = value.ToString();
	}
}