namespace ShopNCook.Views;

public partial class CounterView : ContentView
{
    public static readonly BindableProperty CountProperty =
        BindableProperty.Create(nameof(Count), typeof(uint), typeof(CounterView), default(uint) + 1);

    public static readonly BindableProperty CounterTextProperty =
        BindableProperty.Create(nameof(CounterText), typeof(string), typeof(CounterView), default(string));

    public CounterView()
    {
        InitializeComponent();
    }

    public uint Count
    {
        get => (uint)GetValue(CountProperty);
        set
        {
            SetValue(CountProperty, value <= 1 ? 1 : uint.Parse(value.ToString()));
        }
    }

    public string CounterText
    {
        get => (string)GetValue(CounterTextProperty);
        set
        {
            SetValue(CounterTextProperty, value);
        }
    }

    private void OnPlus(object o, EventArgs e)
    {
        Count += 1;
    }

    private void OnMinus(object o, EventArgs e)
    {
        Count -= 1;
    }
}

