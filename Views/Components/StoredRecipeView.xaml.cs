using Models;

namespace ShopNCook.Views;

public partial class StoredRecipeView : ContentView
{

    private readonly Action<uint> clickCallback;


    public StoredRecipeView(RecipeInfo info, uint personCount, Action<uint> onClickCallback)
    {
        InitializeComponent();

        Note = info.AverageNote;
        BindingContext = info;
        clickCallback = onClickCallback;
        Counter.Count = personCount;
    }

    public float Note
    {
        set => SetNote(value);
    }

    
    private void SetNote(float note)
    {
        note = (uint)note; //truncate integer as we currently do not handle semi stars
        foreach (Image img in Stars.Children.Reverse())
        {
            if (note > 0)
            {
                img.Opacity = 1;
                note--;
            }
            else img.Opacity = 0;
        }
    }

    private void OnRecipeTapped(object sender, TappedEventArgs e)
    {
        clickCallback(Counter.Count);
    }
}