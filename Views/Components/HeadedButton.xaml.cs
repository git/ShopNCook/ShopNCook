namespace ShopNCook.Views;
using Microsoft.Maui.Graphics;

// Classe repr�sentant un bouton avec une t�te (une image en pr�fixe)
public partial class HeadedButton : ContentView
{
    // Texte du bouton
    public string Text
    {
        set => BtnLabel.Text = value;
    }

    // Couleur de l'image en pr�fixe
    public string HeadColor
    {
        set => PrefixBorder.BackgroundColor = Color.FromArgb(value);
    }

    // Source de l'image en pr�fixe
    public string HeadSource
    {
        set => PrefixImage.Source = ImageSource.FromFile(value);
    }

    public HeadedButton()
    {
        InitializeComponent();
    }
}
