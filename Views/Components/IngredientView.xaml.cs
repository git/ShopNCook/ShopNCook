using Models;

namespace ShopNCook.Views;

// Classe repr�sentant une vue d'ingr�dient
public partial class IngredientView : ContentView
{
    // Propri�t�s li�es pour le nom, la quantit� et l'unit� de l'ingr�dient
    private readonly BindableProperty NameProperty =
        BindableProperty.Create(nameof(Name), typeof(string), typeof(IngredientView), default(string));

    public static readonly BindableProperty QuantityProperty =
        BindableProperty.Create(nameof(Quantity), typeof(float), typeof(IngredientView), default(float));

    public static readonly BindableProperty UnitProperty =
        BindableProperty.Create(nameof(Unit), typeof(string), typeof(IngredientView), default(string));

    public string Name
    {
        get => (string)GetValue(NameProperty);
        set => SetValue(NameProperty, value);
    }

    public float Quantity
    {
        get => (float)GetValue(QuantityProperty);
        set => SetValue(QuantityProperty, value);
    }

    public string Unit
    {
        get => (string)GetValue(UnitProperty);
        set => SetValue(UnitProperty, value);
    }

    public IngredientView(Ingredient ingredient)
    {
        InitializeComponent();

        // Initialisation des valeurs de l'ingr�dient
        Name = ingredient.Name;
        Quantity = ingredient.Amount;
        //TODO Unit implementation in IngredientView.xaml.cs
        Unit = ingredient.Unit;
    }
}
