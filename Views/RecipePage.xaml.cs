using ShopNCook.Views;
using System.Windows.Input;
using Models;
using Services;

namespace ShopNCook.Pages;

public partial class RecipePage : ContentPage
{
    private readonly IAccountRecipesPreferencesService preferences;

    private uint note;
    private bool isFavorite;

    public Recipe Recipe { get; init; }


    public ICommand StarCommand => new Command<string>(count => SetNote(uint.Parse(count)));

    public RecipePage(Recipe recipe, IAccountRecipesPreferencesService preferences, uint amount)
    {

        Recipe = recipe;
        BindingContext = this;

        InitializeComponent();

        this.preferences = preferences;

        RecipeRate rate = preferences.GetRate(recipe.Info);

        note = rate.Rate;
        isFavorite = rate.IsFavorite;

        SetFavorite(isFavorite);
        SetNote(note);

        Counter.Count = amount;

        foreach (Ingredient ingredient in recipe.Ingredients)
            IngredientList.Add(new IngredientView(ingredient));

        //retrieves the app's styles
        var styles = Application.Current.Resources.MergedDictionaries.ElementAt(1);

        int count = 0;
        foreach (PreparationStep step in recipe.Steps)
        {
            //TODO display name of PreparationSteps.
            Label label = new Label();
            label.Style = (Style)styles["Small"];
            label.Text = "Step " + ++count + ": " + step.Description;
            StepList.Add(label);
        }
    }

    private void SetNote(uint note)
    {
        this.note = note;
        int i = 1;
        foreach (ImageButton img in Stars.Children)
        {
            if (i <= note)
            {
                img.Source = ImageSource.FromFile("star_full.svg");
                i++;
            }
            else
                img.Source = ImageSource.FromFile("star_empty.svg");
        }
    }

    private void OnFavorite(object o, EventArgs e)
    {
        SetFavorite(!isFavorite);
        if (isFavorite)
            preferences.AddToFavorites(Recipe.Info);
        else
            preferences.RemoveFromFavorites(Recipe.Info);
    }

    private void OnSubmitReviewClicked(object o, EventArgs e)
    {
        preferences.SetReviewScore(Recipe.Info, note);
        UserNotifier.Success("Your review has been successfuly submited");
    }

    private void OnFooterButtonClicked(object o, EventArgs e)
    {
        AddToMyList();
    }

    private void AddToMyList()
    {
        if (!preferences.AddToWeeklyList(Recipe.Info, Counter.Count))
            UserNotifier.Notice("You already added this recipe to you weekly list!");
        else
            UserNotifier.Success("Recipe added to your weekly list.");
    }

    private void SetFavorite(bool isFavorite)
    {
        this.isFavorite = isFavorite;
        if (isFavorite)
            Favorite.Source = ImageSource.FromFile("hearth_on.svg");
        else
            Favorite.Source = ImageSource.FromFile("hearth_off.svg");
    }
    private void OnBackButtonClicked(object sender, EventArgs e)
    {
        Navigation.PopAsync();
    }
}