using Models;

namespace ShopNCook.Pages;

public partial class ProfilePage : ContentPage
{


	public ProfilePage(Account account)
	{
        BindingContext = account;
		InitializeComponent();

        ProfilePicture.Source = ImageSource.FromUri(account.User.ProfilePicture);
    }
    private async void OnBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }
    
}