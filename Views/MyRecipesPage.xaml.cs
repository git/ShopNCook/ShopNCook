using Services;
using Models;
using ShopNCook.Views;

namespace ShopNCook.Pages;

public partial class MyRecipesPage : ContentPage
{

    private IRecipesService service;
    private Account account;

	public MyRecipesPage(Account account, IRecipesService service)
    {
        InitializeComponent();

        this.service = service;
        this.account = account;

        service
            .GetRecipesOf(account)
            .GetAccountRecipes()
            .ForEach(AddRecipeView);
    }

    private void AddRecipeView(RecipeInfo info)
    {
        RecipesLayout.Children.Add(new OwnedRecipeView(info, () =>
        {
            Recipe? recipe = service.GetRecipe(info);
            if (recipe == null)
            {
                UserNotifier.Error("Could not find recipe");
                return;
            }
            IAccountRecipesPreferencesService preferences = service.GetPreferencesOf(account);
            Shell.Current.Navigation.PushAsync(new RecipePage(recipe, preferences, 1));
        },
            () => RemoveRecipe(info)
        ));
    }

    private void RemoveRecipe(RecipeInfo info)
    {
        IAccountOwnedRecipesService recipes = service.GetRecipesOf(account);

        if (!recipes.RemoveRecipe(info))
        {
            UserNotifier.Error("Could not remove recipe");
            return;
        }
        foreach (OwnedRecipeView view in RecipesLayout.Children)
        {
            if (view.IsViewing(info))
            {
                RecipesLayout.Remove(view);
                break;
            }
        }
        UserNotifier.Success("Recipe successfully removed");
    }

    private async void OnBackButtonClicked(object sender, EventArgs e)
    {
        await Navigation.PopAsync();
    }
    private async void OnAddRecipeButtonClicked(object sender, EventArgs e)
    {
        IAccountOwnedRecipesService recipes = service.GetRecipesOf(account);

        var page = new CreateRecipePage(account.User, recipe =>
        {
            if (!recipes.UploadRecipe(recipe))
            {
                UserNotifier.Error("Could not upload recipe.");
                return;
            }
            UserNotifier.Success("Recipe Successfuly uploaded !");
            AddRecipeView(recipe.Info);
            Shell.Current.Navigation.PopAsync(); //go back to current recipe page.
        });
        Shell.Current.Navigation.PushAsync(page); //display RecipePage editor
    }
}