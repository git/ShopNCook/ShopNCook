namespace ShopNCook.Pages;

public partial class Splash : ContentPage
{
	public Splash()
	{
		InitializeComponent();
	}
    private async void OnGetStartedButtonClicked(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("//Login");
    }

}
