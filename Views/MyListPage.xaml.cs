using Services;
using Services;
using Models;
using ShopNCook.Views;

namespace ShopNCook.Pages;

public partial class MyListPage : ContentPage
{

    private readonly IAccountRecipesPreferencesService preferences;
    private readonly IRecipesService service;

	public MyListPage(Account account, IRecipesService service)
    {
		InitializeComponent();

        this.preferences = service.GetPreferencesOf(account);
        this.service = service;

        UpdateMyList();
    }

    private void UpdateMyList()
    {
        RecipesLayout.Children.Clear();
        preferences.GetWeeklyList().ForEach(tuple =>
        {
            RecipeInfo info = tuple.Item1;
            RecipesLayout.Children.Add(new StoredRecipeView(info, tuple.Item2, amount =>
            {
                Recipe recipe = service.GetRecipe(info);
                Shell.Current.Navigation.PushAsync(new RecipePage(recipe, preferences, amount));
            }));
        });
    }

    private async void ContentPage_NavigatedTo(object sender, NavigatedToEventArgs e)
    {
        UpdateMyList();
    }
}