using Services;
using ShopNCook.Controllers;

namespace ShopNCook.Pages;

public partial class LoginPage : ContentPage
{
    private readonly LoginController controller;

    private readonly BindableProperty PasswordHideProperty = BindableProperty.Create(nameof(PasswordHide), typeof(bool), typeof(LoginPage), false);
    public bool PasswordHide { 
        get => (bool) GetValue(PasswordHideProperty);
        private set => SetValue(PasswordHideProperty, value); 
    }

    public LoginPage(LoginController controller)
    {
        InitializeComponent();
        BindingContext = this;
        SetPasswordHide(true);
        this.controller = controller;
    }
    private void OnLoginButtonClicked(object sender, EventArgs e)
    {
        string email = EmailEntry.Text;
        string password = PasswordEntry.Text;
        controller.Login(email, password);
    }

    private async void RegisterLabbelTapped(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("//Register");
    }

    private void OnPasswordRevealClicked(object sender, EventArgs e)
    {
        SetPasswordHide(!PasswordHide);
    }

    public void SetPasswordHide(bool shown)
    {
        PasswordHide = shown;

        if (shown)
            PasswordReveal.Source = ImageSource.FromFile("visibility_off.svg");
        else
            PasswordReveal.Source = ImageSource.FromFile("visibility_on.svg");
    }
}