
namespace ShopNCook.Pages;

using Services;
using Models;
using ShopNCook.Views;

public partial class FavoritesPage : ContentPage
{

    private readonly Account account;
    private IRecipesService service;

    public FavoritesPage(Account account, IRecipesService service)
    {
        InitializeComponent();
        this.account = account;
        this.service = service;

        UpdateFavorites();
    }

    private void UpdateFavorites()
    {
        IAccountRecipesPreferencesService preferences = service.GetPreferencesOf(account);
        RecipeViewLayout.Children.Clear();
        preferences.GetFavorites().ForEach(info =>
        {
            RecipeViewLayout.Children.Add(new RecipeView(info, async () =>
            {
                Recipe recipe = service.GetRecipe(info);
                await Shell.Current.Navigation.PushAsync(new RecipePage(recipe, preferences, 1));
            }));
        });
    }

    private void ContentPage_NavigatedTo(object sender, NavigatedToEventArgs e)
    {
        UpdateFavorites();
    }
}