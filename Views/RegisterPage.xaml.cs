using ShopNCook.Controllers;

namespace ShopNCook.Pages;

public partial class RegisterPage : ContentPage
{
    private readonly RegisterController controller;

    private readonly BindableProperty PasswordHideProperty = BindableProperty.Create(nameof(PasswordHide), typeof(bool), typeof(LoginPage), false);
    public bool PasswordHide
    {
        get => (bool)GetValue(PasswordHideProperty);
        private set => SetValue(PasswordHideProperty, value);
    }


    public RegisterPage(RegisterController controller)
	{
		InitializeComponent();
        BindingContext = this;
        SetPasswordHide(true);
        this.controller = controller;
	}
    private async void LoginTapped(object sender, EventArgs e)
    {
        await Shell.Current.GoToAsync("//Login");
    }
    private async void RegisterTapped(object sender, EventArgs e)
    {
        string email = EmailEntry.Text;
        string password = PasswordEntry.Text;
        string username = UserNameEntry.Text;
        controller.Register(username, email, password);
    }

    private void OnPasswordRevealClicked(object sender, EventArgs e)
    {
        SetPasswordHide(!PasswordHide);
    }

    public void SetPasswordHide(bool shown)
    {
        PasswordHide = shown;

        if (shown)
            PasswordReveal.Source = ImageSource.FromFile("visibility_off.svg");
        else
            PasswordReveal.Source = ImageSource.FromFile("visibility_on.svg");
    }
}