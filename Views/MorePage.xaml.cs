using Models;
using ShopNCook.Controllers;

namespace ShopNCook.Pages;

public partial class MorePage : ContentPage
{

    private readonly MorePageController controller;
    public Account Account { get; private init; }

	public MorePage(Account account, MorePageController controller)
	{
        Account = account;
        BindingContext = this;
        InitializeComponent();
        this.controller = controller;
    }

    private void OnMyRecipesButtonTapped(object sender, EventArgs e)
    {
        controller.GoToMyRecipesPage();
    }
    
    private void OnEditProfileButtonTapped(object sender, EventArgs e)
    {
        controller.GoToProfilePage();
    }
    
    private void OnLogoutButtonTapped(object sender, EventArgs e)
    {
        controller.Logout();
    }
    private async void OnShareButtonClicked(object sender, EventArgs e)
    {
        await Share.RequestAsync(new ShareTextRequest
        {
            Text = "Voici le texte � partager (� changer)",
            Title = "Partagez ce texte : (� modifier)"
        });
    }
}