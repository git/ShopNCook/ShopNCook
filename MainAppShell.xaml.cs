﻿namespace ShopNCook;
using Microsoft.Maui.Controls;
using Models;
using ShopNCook.Controllers;
using ShopNCook.Pages;
using Services;

// Shell principale de l'application après connexion de l'utilisateur
public partial class MainAppShell : Shell
{
    // Constructeur qui prend en argument un compte, un endpoint et une application
    public MainAppShell(Account account, IEndpoint endpoint, IApp app)
	{
		InitializeComponent();

        // Initialisation de chaque onglet avec sa page respective
        HomeTab.ContentTemplate = new DataTemplate(() => new HomePage(account, endpoint));
		FavoritesTab.ContentTemplate = new DataTemplate(() => new FavoritesPage(account, endpoint.RecipesService));
		MyListTab.ContentTemplate = new DataTemplate(() => new MyListPage(account, endpoint.RecipesService));
		MoreTab.ContentTemplate = new DataTemplate(() => new MorePage(account, new MorePageController(account, endpoint, app)));
    }
}
