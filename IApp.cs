﻿using Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShopNCook
{
    // Interface définissant une application.
    // Tout objet implémentant cette interface doit définir la méthode ForceLogin().
    public interface IApp
    {
        public void ForceLogin();
    }
}
