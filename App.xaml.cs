﻿namespace ShopNCook;
using Models;
using Services;
using LocalServices;


// Classe principale de l'application qui implémente l'interface ConnectionObserver et IApp
public partial class App : Application, ConnectionObserver, IApp
{
    // Initialisation de l'interface IEndpoint avec une nouvelle instance de LocalEndpoint
    private IEndpoint Endpoint = new LocalEndpoint();

    public App()
    {
        InitializeComponent();
        ForceLogin(); // Commencer l'application avec l'écran de connexion
    }


    // Méthode appelée lorsque l'utilisateur se connecte avec succès
    public void OnAccountConnected(Account account)
    {
        Shell shell = new MainAppShell(account, Endpoint, this);
        shell.GoToAsync("//Home"); 
        MainPage = shell;
    }

    // Méthode pour forcer l'utilisateur à se connecter
    public void ForceLogin()
    {
        Shell shell = new ConnectAppShell(this, Endpoint.AuthService); 
        shell.GoToAsync("//Splash");
        MainPage = shell;
    }
}