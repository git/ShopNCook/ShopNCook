﻿
using Models;
using LocalServices.Data;
using Moq;
using LocalServices;
using Services;

namespace Tests
{
    public class AuthServiceTests
    {
        private static readonly User SAMPLE_USER = new User(new Uri("https://www.referenseo.com/wp-content/uploads/2019/03/image-attractive-960x540.jpg"), "user", Guid.NewGuid());
        private static readonly Account SAMPLE_ACC = new Account(SAMPLE_USER, "mail");

        [Fact]
        public void TestLogin()
        {
            var database = new Mock<IDatabase>();
            database.Setup(x => x.GetAccount("mail", "1234")).Returns(SAMPLE_ACC);
            var service = new AuthService(database.Object);
            var acc = service.Login("mail", "1234");
            Assert.Equal(acc, SAMPLE_ACC);
        }

        [Fact]
        public void TestRegister()
        {
            var database = new Mock<IDatabase>();
            database.Setup(x => x.GetAccount("mail", "1234")).Returns(SAMPLE_ACC);
            var service = new AuthService(database.Object);
            var acc = service.Register("mail", "foo", "1234");
            Assert.Equal(acc, new Account(new User(Constants.DEFAULT_ACCOUNT_IMAGE, "foo", acc.User.Id), "mail"));
        }
    }
}
