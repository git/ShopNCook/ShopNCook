﻿using LocalServices.Data;
using Models;
using LocalServices;
using Services;
using System.Collections.Immutable;

namespace Tests
{
    public class RecipeServicesTests
    {
        private static readonly User SAMPLE_USER = new User(new Uri("https://www.referenseo.com/wp-content/uploads/2019/03/image-attractive-960x540.jpg"), "user", Guid.NewGuid());
        private static readonly Account SAMPLE_ACC = new Account(SAMPLE_USER, "mail");
        private static readonly Recipe SAMPLE_RECIPE = new RecipeBuilder("foo", SAMPLE_USER).Build();

        [Fact]
        public void GetRecipe()
        {
            var database = new Mock<IDatabase>();
            database.Setup(x => x.GetRecipe(SAMPLE_RECIPE.Info.Id)).Returns(SAMPLE_RECIPE);
            var service = new RecipesService(database.Object);
            Assert.Equal(service.GetRecipe(SAMPLE_RECIPE.Info), SAMPLE_RECIPE);
        }

    }
}
