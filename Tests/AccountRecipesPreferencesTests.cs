﻿using LocalServices;
using LocalServices.Data;
using Models;
using System.Collections.Immutable;

namespace Tests
{
    public class AccountRecipesPreferencesTests
    {
        private static readonly User SAMPLE_USER = new User(new Uri("https://www.referenseo.com/wp-content/uploads/2019/03/image-attractive-960x540.jpg"), "user", Guid.NewGuid());
        private static readonly Account SAMPLE_ACC = new Account(SAMPLE_USER, "mail");
        private static readonly Recipe SAMPLE_RECIPE = new RecipeBuilder("foo", SAMPLE_USER).Build();

        [Fact]
        public void Review()
        {
            var fav_inserted = false;
            var rate_inserted = false;
            var dict = new Dictionary<Guid, RecipeRate>();
            var db = new Mock<IDatabase>();
            db.Setup(x => x.ListRatesOf(SAMPLE_USER.Id)).Returns(() => dict.ToImmutableDictionary());
            db.Setup(x => x.InsertRate(SAMPLE_USER.Id, SAMPLE_RECIPE.Info.Id, new RecipeRate(true, 0))).Callback(() => fav_inserted = true);
            db.Setup(x => x.InsertRate(SAMPLE_USER.Id, SAMPLE_RECIPE.Info.Id, new RecipeRate(false, 3))).Callback(() => rate_inserted = true);
            var pref = new AccountRecipesPreferences(SAMPLE_ACC, db.Object);
            pref.AddToFavorites(SAMPLE_RECIPE.Info);
            pref.SetReviewScore(SAMPLE_RECIPE.Info, 3);
            Assert.True(fav_inserted);
            Assert.True(rate_inserted);
        }

        public void AddWeeklyList()
        {
            var inserted = false;

            var dict = new Dictionary<Guid, uint> { };
            dict.Add(SAMPLE_RECIPE.Info.Id, 88);
            var db = new Mock<IDatabase>();
            db.Setup(x => x.GetRecipeListOf(SAMPLE_USER.Id)).Returns(() => dict);
            db.Setup(x => x.InsertInUserList(SAMPLE_USER.Id, SAMPLE_RECIPE.Info.Id, 88)).Callback(() => inserted = true);
            var pref = new AccountRecipesPreferences(SAMPLE_ACC, db.Object);
            pref.AddToWeeklyList(SAMPLE_RECIPE.Info, 88);
            Assert.True(inserted);
            Assert.True(pref.GetWeeklyList().Contains((SAMPLE_RECIPE.Info, 88)));
        }
    }
}
