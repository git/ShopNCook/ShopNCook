﻿using LocalServices.Data;
using Models;
using Services;
using System.Collections.Immutable;

namespace Tests
{
    public class AccountOwnedRecipesTest
    {
        private static readonly User SAMPLE_USER = new User(new Uri("https://www.referenseo.com/wp-content/uploads/2019/03/image-attractive-960x540.jpg"), "user", Guid.NewGuid());
        private static readonly Account SAMPLE_ACC = new Account(SAMPLE_USER, "mail");
        private static readonly Recipe SAMPLE_RECIPE = new RecipeBuilder("foo", SAMPLE_USER).Build();

        [Fact]
        public void UploadRemove()
        {
            var db = new Mock<IDatabase>();
            db.Setup(x => x.ListAllRecipes()).Returns(() => new List<Recipe>().ToImmutableList());
            var owned = new AccountOwnedRecipes(SAMPLE_ACC, db.Object);
            owned.UploadRecipe(SAMPLE_RECIPE);
            Assert.Contains(SAMPLE_RECIPE.Info, owned.GetAccountRecipes());
            owned.RemoveRecipe(SAMPLE_RECIPE.Info);
            Assert.DoesNotContain(SAMPLE_RECIPE.Info, owned.GetAccountRecipes());
        }
    }
}
