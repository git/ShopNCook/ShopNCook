﻿using Models;

namespace LocalServices
{
    public class Constants
    {
        public static readonly Uri DEFAULT_ACCOUNT_IMAGE = new Uri("https://www.pngkey.com/png/full/115-1150152_default-profile-picture-avatar-png-green.png");
    }
}
