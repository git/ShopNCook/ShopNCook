﻿using Services;
using LocalServices.Data;
using Models;
using System.Collections.Immutable;

namespace LocalServices
{
    public class RecipesService : IRecipesService
    {

        private readonly IDatabase db;
        private readonly Dictionary<Account, AccountServices> accountsData = new Dictionary<Account, AccountServices>();

        public RecipesService(IDatabase db)
        {
            this.db = db;
        }

        public ImmutableList<RecipeInfo> PopularRecipes()
        {
            return db.ListAllRecipes().Take(4).ToImmutableList().ConvertAll(v => v.Info);
        }

        public Recipe? GetRecipe(RecipeInfo info)
        {
            return db.GetRecipe(info.Id);
        }


        public IAccountOwnedRecipesService GetRecipesOf(Account account)
        {
            return GetOrInitData(account).Recipes;
        }
        public IAccountRecipesPreferencesService GetPreferencesOf(Account account)
        {
            return GetOrInitData(account).Preferences;
        }

        private AccountServices GetOrInitData(Account account)
        {
            AccountServices? data;
            accountsData.TryGetValue(account, out data);

            if (data == null)
            {
                AccountOwnedRecipes recipes = new AccountOwnedRecipes(account, db);
                AccountRecipesPreferences preferences = new AccountRecipesPreferences(account, db);
                data = new AccountServices(recipes, preferences);
                accountsData.Add(account, data);
            }
            return data;
        }

        public ImmutableList<RecipeInfo> SearchRecipes(string prompt)
        {
            return db.ListAllRecipes()
                .ConvertAll(r => r.Info)
                .FindAll(i => i.Name.ToLower().Contains(prompt.ToLower()));
        }
    }
}
