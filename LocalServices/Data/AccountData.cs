﻿using System.Runtime.Serialization;

namespace LocalServices.Data
{
    [DataContract]
    internal record AccountData(
        [property: DataMember] Guid UserId,
        [property: DataMember] string Email,
        [property: DataMember] string PasswordHash
    );
}
