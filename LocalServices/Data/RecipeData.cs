﻿using Models;
using System.Collections.Immutable;
using System.Runtime.Serialization;

namespace LocalServices.Data
{
    [DataContract]
    internal record RecipeData(
        [property: DataMember] RecipeInfo Info,
        [property: DataMember] Guid OwnerID,
        [property: DataMember] ImmutableList<Ingredient> Ingredients,
        [property: DataMember] ImmutableList<PreparationStep> Steps
    );
}