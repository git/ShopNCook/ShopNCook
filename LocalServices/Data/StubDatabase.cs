﻿using LocalServices.Data;
using Models;
using System.Collections.Immutable;

namespace LocalServices.Data
{
    /// <summary>
    /// stub implementation for IDatabase
    /// </summary>
    internal class StubDatabase : IDatabase
    {
        private readonly User User = new User(new Uri("https://images.pexels.com/photos/37546/woman-portrait-face-studio-37546.jpeg?cs=srgb&dl=beauty-face-headshot-37546.jpg&fm=jpg"), "David", Guid.NewGuid());
        public Account? GetAccount(string email, string passwordHash)
        {
            return new Account(User, "david@gmail.com");
        }

        public Recipe? GetRecipe(Guid id)
        {
            return new Recipe(new RecipeInfo("Foo", 4, 5, new Uri("https://th.bing.com/th/id/R.e57009c4044bef3e86f8a7b18a7cf36a?rik=K3dSu3KoTgeRSw&pid=ImgRaw&r=0"), 4.5F, id), User, new List<Ingredient> { new Ingredient("Chocolate", 4, "g") }.ToImmutableList(), new List<PreparationStep> { new PreparationStep("Eat Chocolate", "Eat the chocolate") }.ToImmutableList());
        }

        public ImmutableDictionary<Guid, uint> GetRecipeListOf(Guid user)
        {
            return new Dictionary<Guid, uint>().ToImmutableDictionary();
        }

        public RecipeRate GetRecipeRate(Guid user, Guid recipe)
        {
            return new RecipeRate(true, 4);
        }

        public void InsertAccount(Account account, string passwordHash)
        {
            
        }

        public void InsertInUserList(Guid userId, Guid recipeId, uint persAmount)
        {
        }

        public void InsertRate(Guid userId, Guid recipeId, RecipeRate rate)
        {
        }

        public void InsertRecipe(Recipe recipe)
        {
        }

        public void InsertUser(User user)
        {
        }

        public ImmutableList<Recipe> ListAllRecipes()
        {
            return new List<Recipe>().ToImmutableList();
        }

        public ImmutableDictionary<Guid, RecipeRate> ListRatesOf(Guid user)
        {
            return new Dictionary<Guid, RecipeRate>().ToImmutableDictionary();
        }

        public void RemoveFromUserList(Guid userId, Guid recipeId)
        {
        }

        public void RemoveRecipe(Guid id)
        {
        }
    }
}
