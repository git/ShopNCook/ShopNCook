﻿
using Models;
using System.Collections.Immutable;

namespace LocalServices.Data
{
    /// <summary>
    /// The database interface defines all the different kinds of requests the LocalEndpoint needs to store and retrieve data.
    /// </summary>
    public interface IDatabase
    {
        /// <summary>
        /// Get a Recipe from its identifier
        /// </summary>
        /// <param name="id"></param>
        /// <returns>The recipe if the identifier is registered in the database</returns>

        public Recipe? GetRecipe(Guid id);
        /// <summary>
        ///  Get the rate of a user for a given recipe
        /// </summary>
        /// <param name="user">The user identifier</param>
        /// <param name="recipe">The recipe identifier</param>
        /// <returns>Returns a rate</returns>
        public RecipeRate GetRecipeRate(Guid user, Guid recipe);

        /// <summary>
        /// Gets an account from an email and a password hash
        /// </summary>
        /// <param name="email"></param>
        /// <param name="passwordHash"></param>
        /// <returns>some account if the email and the hash is found in the database</returns>
        public Account? GetAccount(string email, string passwordHash);

        /// <summary>
        /// Insert a recipe in user's weekly list 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="recipeId"></param>
        /// <param name="persAmount"></param>
        public void InsertInUserList(Guid userId, Guid recipeId, uint persAmount);

        /// <summary>
        /// Remove a recipe from user's weekly list
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="recipeId"></param>
        public void RemoveFromUserList(Guid userId, Guid recipeId);

        /// <summary>
        /// Inserts an account in the database, with the given passwordhash
        /// </summary>
        /// <param name="account"></param>
        /// <param name="passwordHash"></param>
        public void InsertAccount(Account account, string passwordHash);

        /// <summary>
        /// Inserts a recipe
        /// </summary>
        /// <param name="recipe"></param>
        public void InsertRecipe(Recipe recipe);

        /// <summary>
        /// Inserts a user
        /// </summary>
        /// <param name="user"></param>
        public void InsertUser(User user);

        /// <summary>
        /// Inserts a user rate over the given recipe identifier
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="recipeId"></param>
        /// <param name="rate"></param>
        public void InsertRate(Guid userId, Guid recipeId, RecipeRate rate);

        /// <summary>
        /// Removes a recipe
        /// </summary>
        /// <param name="id"></param>
        public void RemoveRecipe(Guid id);

        /// <summary>
        /// Lists all recipes in the database
        /// </summary>
        /// <returns></returns>
        public ImmutableList<Recipe> ListAllRecipes();

        /// <summary>
        /// List the ratings of an user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ImmutableDictionary<Guid, RecipeRate> ListRatesOf(Guid user);

        /// <summary>
        /// Get the weekly list of given user
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public ImmutableDictionary<Guid, uint> GetRecipeListOf(Guid user);
    }
}
