﻿

using Models;
using System.Runtime.Serialization;

namespace LocalServices.Data
{
    [DataContract]
    internal record UserData(
        [property: DataMember] User User,
        [property: DataMember] Dictionary<Guid, RecipeRate> Rates,
        [property: DataMember] Dictionary<Guid, uint> RecipesList
    );
}
