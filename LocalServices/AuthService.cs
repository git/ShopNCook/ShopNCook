﻿using Models;
using Services;
using LocalServices.Data;

namespace LocalServices
{
    public class AuthService : IAuthService
    {
        private readonly IDatabase db;


        public AuthService(IDatabase db)
        {
            this.db = db;
        }

        public Account? Login(string email, string password)
        {
            return db.GetAccount(email, password);
        }

        public Account? Register(string email, string username, string password)
        {
            if (email == null || username == null || password == null)
                return null;

            var userAccount = new Account(new User(Constants.DEFAULT_ACCOUNT_IMAGE, username, Guid.NewGuid()), email);
            db.InsertAccount(userAccount, password);
            return userAccount;
        }
    }
}
