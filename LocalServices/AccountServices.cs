﻿using Services;

namespace LocalServices
{
    internal record AccountServices(IAccountOwnedRecipesService Recipes, IAccountRecipesPreferencesService Preferences);
}
