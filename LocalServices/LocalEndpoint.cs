﻿using Services;
using LocalServices.Data;
using Models;
using System.Collections.Immutable;

namespace LocalServices
{

    /// <summary>
    /// The local endpoint is an implementation of the Services API.
    /// This class is the _entry point_ class of the implementation
    /// </summary>
    public class LocalEndpoint : IEndpoint
    {
        private readonly IAuthService authService;
        private readonly IRecipesService recipesService;


        public LocalEndpoint()
        {
            var db = new CatastrophicPerformancesDatabase(Environment.GetFolderPath(Environment.SpecialFolder.Personal));


            if (db.IsEmpty())
                PrepareDatabase(db);

            recipesService = new RecipesService(db);
            authService = new AuthService(db);
        }

        public IAuthService AuthService => authService;

        public IRecipesService RecipesService => recipesService;


        /// <summary>
        /// Inserts sample data in the local database
        /// </summary>
        /// <param name="db"></param>
        private static void PrepareDatabase(IDatabase db)
        {
            User USER1 = new User(new Uri("https://i.ibb.co/L6t6bGR/DALL-E-2023-05-10-20-27-31-cook-looking-at-the-camera-with-a-chef-s-hat-laughing-in-an-exaggerated-w.png"), "The Funny Chief", MakeGuid(1));
            User USER2 = new User(Constants.DEFAULT_ACCOUNT_IMAGE, "Yanis", MakeGuid(2));
            User USER3 = new User(Constants.DEFAULT_ACCOUNT_IMAGE, "Leo", MakeGuid(3));

            db.InsertUser(USER1);
            db.InsertUser(USER2);
            db.InsertUser(USER3);

            db.InsertAccount(new Account(USER1, "chief@cook.com"), "123456");
            db.InsertAccount(new Account(USER2, "yanis@google.com"), "123456");
            db.InsertAccount(new Account(USER3, "leo@google.com"), "123456");

            db.InsertRecipe(
                new RecipeBuilder("Fried Chicken", USER3)
                    .SetImage(new Uri("https://www.foodandwine.com/thmb/JMrJBrYh3fxDRgkV24_8dZH_zpQ=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/FAW-recipes-crispy-buttermilk-fried-chicken-hero-04-3a32f9a4a1984ecab79fb28e93d4bc00.jpg"))
                    .AddIngredient(new Ingredient("pasta (your choice)", 225, "g"))
                    .AddIngredient(new Ingredient("shrimp, peeled and deveined", 450, "g"))
                    .AddIngredient(new Ingredient("tablespoons butter", 200, "g"))
                    .AddIngredient(new Ingredient("Salt and pepper to taste", 3, "g"))
                    .AddIngredient(new Ingredient("Fresh parsley, chopped (for garnish)", 50, "g"))
                    .AddStep(new PreparationStep("Step 1", "Cook the pasta according to the package instructions until al dente. Drain and set aside."))
                    .AddStep(new PreparationStep("Step 2", "In a large skillet, melt the butter over medium heat. Add the minced garlic and cook for about 1 minute until fragrant."))
                    .AddStep(new PreparationStep("Step 3", "Add the shrimp to the skillet and season with salt and pepper. Cook for 2-3 minutes on each side until pink and cooked through."))
                    .AddStep(new PreparationStep("Step 4", "Add the cooked pasta to the skillet with the shrimp and toss everything together until well combined and coated with the garlic butter sauce."))
                    .AddStep(new PreparationStep("Step 5", "Remove from heat and garnish with freshly chopped parsley."))
                    .AddStep(new PreparationStep("Step 6", "Serve hot and enjoy!"))
                    .SetCallPerPers(500)
                    .SetCookTimeMins(25)
                    .Build()
                );
            db.InsertRecipe(
                new RecipeBuilder("Chicken Caesar Salad", USER2)
                    .SetImage(new Uri("https://www.jessicagavin.com/wp-content/uploads/2022/06/chicken-caesar-salad-28-1200.jpg"))
                    .AddIngredient(new Ingredient("boneless, skinless chicken breasts", 180, "g"))
                    .AddIngredient(new Ingredient("romaine lettuce, chopped", 40, "g"))
                    .AddIngredient(new Ingredient("croutons", 100, "g"))
                    .AddIngredient(new Ingredient("Parmesan cheese, grated", 240, "g"))
                    .AddIngredient(new Ingredient("Caesar dressing", 25, "g"))
                    .AddStep(new PreparationStep("Step 1", "Preheat the grill or stovetop pan over medium heat."))
                    .AddStep(new PreparationStep("Step 2", "Season the chicken breasts with salt and pepper."))
                    .AddStep(new PreparationStep("Step 3", "Grill or cook the chicken breasts until they reach an internal temperature of 165°F (74°C) and are cooked through."))
                    .AddStep(new PreparationStep("Step 4", "Let the chicken breasts rest for a few minutes, then slice them into thin strips."))
                    .AddStep(new PreparationStep("Step 5", "In a large bowl, combine the chopped romaine lettuce, croutons, and grated Parmesan cheese."))
                    .AddStep(new PreparationStep("Step 6", "Add the sliced chicken on top of the salad."))
                    .AddStep(new PreparationStep("Step 7", "Pour the Caesar dressing over the salad and toss everything together until well coated."))
                    .AddStep(new PreparationStep("Step 8", "Serve immediately and enjoy!"))
                    .SetCallPerPers(300)
                    .SetCookTimeMins(30)
                    .Build()
                );

            db.InsertRecipe(
                new RecipeBuilder("Chocolate Chip Cookies", USER3)
                    .SetImage(new Uri("https://images.aws.nestle.recipes/original/5b069c3ed2feea79377014f6766fcd49_Original_NTH_Chocolate_Chip_Cookie.jpg"))
                    .AddIngredient(new Ingredient("all-purpose flour", 50, "g"))
                    .AddIngredient(new Ingredient("baking soda", 400, "cl"))
                    .AddIngredient(new Ingredient("salt", 10, "g"))
                    .AddIngredient(new Ingredient("unsalted butter, softened", 1, "g"))
                    .AddIngredient(new Ingredient("granulated sugar", 250, "g"))
                    .AddIngredient(new Ingredient("packed brown sugar", 200, "g"))
                    .AddIngredient(new Ingredient("vanilla extract", 50, "g"))
                    .AddIngredient(new Ingredient("large eggs", 100, "g"))
                    .AddIngredient(new Ingredient("chocolate chips", 75, "g"))
                    .AddStep(new PreparationStep("Step 1", "Preheat the oven to 375°F (190°C)."))
                    .AddStep(new PreparationStep("Step 2", "In a medium bowl, whisk together the flour, baking soda, and salt."))
                    .AddStep(new PreparationStep("Step 3", "In a large bowl, cream together the softened butter, granulated sugar, and brown sugar until light and fluffy."))
                    .AddStep(new PreparationStep("Step 4", "Beat in the vanilla extract and eggs, one at a time, until well combined."))
                    .AddStep(new PreparationStep("Step 5", "Gradually add the flour mixture to the butter mixture, mixing until just combined."))
                    .AddStep(new PreparationStep("Step 6", "Stir in the chocolate chips."))
                    .AddStep(new PreparationStep("Step 7", "Drop rounded tablespoons of cookie dough onto ungreased baking sheets."))
                    .AddStep(new PreparationStep("Step 8", "Bake in the preheated oven for 9-11 minutes or until golden brown around the edges."))
                    .AddStep(new PreparationStep("Step 9", "Remove from the oven and let the cookies cool on the baking sheets for a few minutes."))
                    .AddStep(new PreparationStep("Step 10", "Transfer the cookies to a wire rack to cool completely."))
                    .AddStep(new PreparationStep("Step 11", "Serve and enjoy these delicious chocolate chip cookies!"))
                    .SetCallPerPers(24)
                    .SetCookTimeMins(20)
                    .Build()
                );
            db.InsertRecipe(
                new RecipeBuilder("Garlic Butter Shrimp Pasta", USER3)
                    .SetImage(new Uri("https://amyinthekitchen.com/wp-content/uploads/2022/05/Pasta-with-garlic-butter-shrimp-recipe.jpg"))
                    .AddIngredient(new Ingredient("pasta (your choice)", 225, "g"))
                    .AddIngredient(new Ingredient("shrimp, peeled and deveined", 450, "g"))
                    .AddIngredient(new Ingredient("butter", 200, "g"))
                    .AddIngredient(new Ingredient("salt and pepper to taste", 3, "g"))
                    .AddIngredient(new Ingredient("fresh parsley, chopped (for garnish)", 50, "g"))
                    .AddStep(new PreparationStep("Step 1", "Cook the pasta according to the package instructions until al dente. Drain and set aside."))
                    .AddStep(new PreparationStep("Step 2", "In a large skillet, melt the butter over medium heat. Add the minced garlic and cook for about 1 minute until fragrant."))
                    .AddStep(new PreparationStep("Step 3", "Add the shrimp to the skillet and season with salt and pepper. Cook for 2-3 minutes on each side until pink and cooked through."))
                    .AddStep(new PreparationStep("Step 4", "Add the cooked pasta to the skillet with the shrimp and toss everything together until well combined and coated with the garlic butter sauce."))
                    .AddStep(new PreparationStep("Step 5", "Remove from heat and garnish with freshly chopped parsley."))
                    .AddStep(new PreparationStep("Step 6", "Serve hot and enjoy!"))
                    .SetCallPerPers(500)
                    .SetCookTimeMins(25)
                    .Build()
                );
            db.InsertRecipe(
                new RecipeBuilder("Caprese Salad", USER1)
                    .SetImage(new Uri("https://natashaskitchen.com/wp-content/uploads/2019/08/Caprese-Salad-6.jpg"))
                    .AddIngredient(new Ingredient("fresh mozzarella cheese", 250, "g"))
                    .AddIngredient(new Ingredient("ripe tomatoes", 3, "g"))
                    .AddIngredient(new Ingredient("fresh basil leaves", 50, "g"))
                    .AddIngredient(new Ingredient("extra virgin olive oil", 30, "cl"))
                    .AddIngredient(new Ingredient("balsamic glaze", 120, "g"))
                    .AddIngredient(new Ingredient("salt and pepper to taste", 200, "g"))
                    .AddStep(new PreparationStep("Step 1", "Slice the fresh mozzarella cheese and ripe tomatoes into ¼-inch thick slices."))
                    .AddStep(new PreparationStep("Step 2", "Arrange the tomato and mozzarella slices on a serving platter, alternating them."))
                    .AddStep(new PreparationStep("Step 3", "Tuck the fresh basil leaves between the tomato and mozzarella slices."))
                    .AddStep(new PreparationStep("Step 4", "Drizzle the extra virgin olive oil and balsamic glaze over the salad."))
                    .AddStep(new PreparationStep("Step 5", "Season with salt and pepper to taste."))
                    .AddStep(new PreparationStep("Step 6", "Let the salad sit for a few minutes to allow the flavors to meld together."))
                    .AddStep(new PreparationStep("Step 7", "Serve the Caprese salad as a refreshing appetizer or side dish. Enjoy!"))
                    .SetCallPerPers(200)
                    .SetCookTimeMins(10)
                    .Build()
                );
            db.InsertRecipe(
               new RecipeBuilder("Chicken Parmesan", USER2)
                    .SetImage(new Uri("https://tastesbetterfromscratch.com/wp-content/uploads/2023/03/Chicken-Parmesan-1.jpg"))
                    .AddIngredient(new Ingredient("boneless, skinless chicken breasts", 10, "g"))
                    .AddIngredient(new Ingredient("all-purpose flour", 125, "g"))
                    .AddIngredient(new Ingredient("eggs, beaten", 300, "g"))
                    .AddIngredient(new Ingredient("breadcrumbs", 50, "g"))
                    .AddIngredient(new Ingredient("grated Parmesan cheese", 10, "g"))
                    .AddIngredient(new Ingredient("marinara sauce", 30, "g"))
                    .AddIngredient(new Ingredient("shredded mozzarella cheese", 40, "g"))
                    .AddIngredient(new Ingredient("olive oil", 250, "cl"))
                    .AddIngredient(new Ingredient("salt and pepper to taste", 20, "g"))
                    .AddStep(new PreparationStep("Step 1", "Preheat the oven to 375°F (190°C)."))
                    .AddStep(new PreparationStep("Step 2", "Season the chicken breasts with salt and pepper."))
                    .AddStep(new PreparationStep("Step 3", "Dredge each chicken breast in flour, dip in beaten eggs, and coat with breadcrumbs mixed with grated Parmesan cheese."))
                    .AddStep(new PreparationStep("Step 4", "Heat olive oil in a large skillet over medium-high heat."))
                    .AddStep(new PreparationStep("Step 5", "Cook the breaded chicken breasts in the skillet for about 3-4 minutes per side until golden brown."))
                    .AddStep(new PreparationStep("Step 6", "Transfer the chicken breasts to a baking dish."))
                    .AddStep(new PreparationStep("Step 7", "Pour marinara sauce over the chicken breasts, spreading it evenly."))
                    .AddStep(new PreparationStep("Step 8", "Sprinkle shredded mozzarella cheese over the top of each chicken breast."))
                    .AddStep(new PreparationStep("Step 9", "Bake in the preheated oven for 20-25 minutes or until the chicken is cooked through and the cheese is melted and bubbly."))
                    .AddStep(new PreparationStep("Step 10", "Remove from the oven and let it cool for a few minutes."))
                    .AddStep(new PreparationStep("Step 11", "Serve the chicken Parmesan hot with a side of pasta or a salad. Enjoy!"))
                    .SetCallPerPers(4)
                    .SetCookTimeMins(45)
                    .Build()
                );

            db.InsertRecipe(
               new RecipeBuilder("Vegetable Curry", USER1)
                .SetImage(new Uri("https://images.immediate.co.uk/production/volatile/sites/30/2020/08/recipe-image-legacy-id-402452_12-d37f0f4.jpg?quality=90&webp=true&resize=300,272"))
                .AddIngredient(new Ingredient("onion, chopped", 60, "g"))
                .AddIngredient(new Ingredient("garlic cloves, minced", 132, "g"))
                .AddIngredient(new Ingredient("ginger, grated", 120, "g"))
                .AddIngredient(new Ingredient("mixed vegetables (e.g., carrots, peas, potatoes)", 80, "g"))
                .AddIngredient(new Ingredient("coconut milk", 30, "cl"))
                .AddIngredient(new Ingredient("curry powder", 40, "g"))
                .AddIngredient(new Ingredient("turmeric powder", 30, "g"))
                .AddIngredient(new Ingredient("cumin powder", 60, "g"))
                .AddIngredient(new Ingredient("coriander powder", 20, "g"))
                .AddIngredient(new Ingredient("red chili flakes (optional)", 50, "g"))
                .AddIngredient(new Ingredient("salt to taste", 10, "g"))
                .AddIngredient(new Ingredient("fresh cilantro leaves, chopped (for garnish)", 20, "g"))
                .AddStep(new PreparationStep("Step 1", "In a large pan or pot, heat oil over medium heat."))
                .AddStep(new PreparationStep("Step 2", "Add the chopped onion and cook until translucent."))
                .AddStep(new PreparationStep("Step 3", "Add the minced garlic and grated ginger. Sauté for a minute until fragrant."))
                .AddStep(new PreparationStep("Step 4", "Add the mixed vegetables to the pan and cook for a few minutes until slightly tender."))
                .AddStep(new PreparationStep("Step 5", "In a small bowl, mix together the curry powder, turmeric powder, cumin powder, coriander powder, red chili flakes (if using), and salt."))
                .AddStep(new PreparationStep("Step 6", "Add the spice mixture to the pan with vegetables and stir well to coat evenly."))
                .AddStep(new PreparationStep("Step 7", "Pour in the coconut milk and stir to combine."))
                .AddStep(new PreparationStep("Step 8", "Simmer the vegetable curry over low heat for 15-20 minutes, stirring occasionally, until the flavors meld together and the vegetables are cooked through."))
                .AddStep(new PreparationStep("Step 9", "Garnish with freshly chopped cilantro leaves."))
                .AddStep(new PreparationStep("Step 10", "Serve the vegetable curry hot over steamed rice or with naan bread. Enjoy!"))
                .SetCallPerPers(300)
                .SetCookTimeMins(30)
                .Build()
                );
        }

        /// <summary>
        /// helper function to Create a Guid from a given seed
        /// </summary>
        /// <param name="seed">the seed to use for the generation</param>
        /// <returns></returns>
        private static Guid MakeGuid(int seed)
        {
            var r = new Random(seed);
            var guid = new byte[16];
            r.NextBytes(guid);

            return new Guid(guid);
        }
    }
}